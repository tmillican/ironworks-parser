### IronWorks.Parser

## Project Description

__IronWorks.Parser__ is a collection of parsers that produce
IronWorks.Structures concrete data structures from the raw files and data
streams of FINAL FANTASY XIV: A Realm Reborn.

## License

__IronWorks.Parser__ is released under the MIT License ([SPDX MIT][3]). A
[markdown version][4] of the license is provided in the repository.

[3]:https://spdx.org/licenses/MIT.html
[4]:LICENSE.md
