namespace IronWorks.Parser.Test
{
    using System;
    using System.IO;

    using global::Kaitai;
    using FluentAssertions;
    using Xunit;

    using IK = IronWorks.Kaitai;
    using IronWorks.Parser;

    /// <summary>
    ///   Tests for <see cref="SqPackParser"/>.
    /// </summary>
    public static class SqPackParserTests
    {
        // ISqPack Parse(
        //     string indexFileName,
        //     SqPackIndexFormat indexFormat,
        //     bool makeMutable = false)
        // ---------------------------------------------------------------------

        [Fact]
        public static void
        Parse_ShouldThrowArgumentNullException_WithNullIndexFileName()
        {
            Action parse = () => SqPackParser.Parse(
                null, SqPackIndexFormat.Format1);

            parse.Should().Throw<ArgumentNullException>(
                "because 'indexFileName' is a required parameter")
                .And.ParamName.Should().Be(
                    "indexFileName",
                    "because we describe our exceptions");
        }

        [Fact]
        public static void
        Parse_ShouldThrowArgumentException_WithUndefinedIndexFormat()
        {
            Action parse = () => SqPackParser.Parse(
                "foo", (SqPackIndexFormat)5);
            var expectedMessage = string.Format(
                Strings.Argument_UnsupportedIndexFormat, 5);

            parse.Should().Throw<ArgumentException>(
                "because 5 is not a valid index type")
                .WithMessage(
                    $"{expectedMessage}*",
                    "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "indexFormat",
                    "because we fully describe our exceptions");
        }

        // ISqPackFile VisitFileDescriptor(
        //     IK.SqPackIndex.FileTable.Descriptor node,
        //     bool makeMutable = false)
        // ---------------------------------------------------------------------

        [Fact]
        public static void
        VisitFileDescriptor_ShouldThrowArgumentNullException_WithNullNode()
        {
            Action visit = () =>
                SqPackParser.VisitFileDescriptor(null);

            visit.Should().Throw<ArgumentNullException>(
                "because 'node' is a required parameter")
                .And.ParamName.Should().Be(
                    "node",
                    "because we describe our exceptions");
        }

        // ISqPackFolder VisitFileTable(
        //     IK.SqPackIndex.FileTable node,
        //     long nameHash,
        //     bool makeMutable = false)
        // ---------------------------------------------------------------------

        [Fact]
        public static void
        VisitFileTable_ShouldThrowArgumentNullException_WithNullNode()
        {
            Action visit = () =>
                SqPackParser.VisitFileTable(null, 0);

            visit.Should().Throw<ArgumentNullException>(
                "because 'node' is a required parameter")
                .And.ParamName.Should().Be(
                    "node",
                    "because we describe our exceptions");
        }

        [Theory]
        [InlineData(-1)]
        [InlineData((long)uint.MaxValue + 1)]
        public static void
        VisitFileTable_ShouldThrowArgumentOutOfRangeException_WithOutOfRangeNameHash(
            long nameHash)
        {
            Action visit = () =>
                SqPackParser.VisitFileTable(
                    new IK.SqPackIndex(
                        1,
                        new KaitaiStream("../../Data/good.index"))
                    .FileSegment,
                    nameHash);
            var expectedMessage = string.Format(
                Strings.ArgumentOutOfRange_Range, 0, uint.MaxValue);

            visit.Should().Throw<ArgumentException>(
                "because 'nameHash' is an unsigned 32-bit integer value")
                .WithMessage(
                    $"{expectedMessage}*",
                    "because we fully describe our exceptions")
                .And.ParamName.Should().Be(
                    "nameHash",
                    "because we fully describe our exceptions");
        }

        // ISqPackFolder VisitFolderDescriptor(
        //     IK.SqPackIndex.FolderTable.Descriptor node,
        //     bool makeMutable = false)
        // ---------------------------------------------------------------------

        [Fact]
        public static void
        VisitFolderDescriptor_ShouldThrowArgumentNullException_WithNullNode()
        {
            Action visit = () =>
                SqPackParser.VisitFolderDescriptor(null);

            visit.Should().Throw<ArgumentNullException>(
                "because 'node' is a required parameter")
                .And.ParamName.Should().Be(
                    "node",
                    "because we describe our exceptions");
        }

        // ISqPack VisitIndex(
        //     IK.SqPackIndex node,
        //     FileInfo indexInfo,
        //     bool makeMutable = false)
        // ---------------------------------------------------------------------

        [Fact]
        public static void
        VisitIndex_ShouldThrowArgumentNullException_WithNullNode()
        {
            Action visit = () =>
                SqPackParser.VisitIndex(null, new FileInfo("/"));

            visit.Should().Throw<ArgumentNullException>(
                "because 'node' is a required parameter")
                .And.ParamName.Should().Be(
                    "node",
                    "because we describe our exceptions");
        }

        [Fact]
        public static void
        VisitIndex_ShouldThrowArgumentNullException_WithNullIndexFileInfo()
        {
            var indexNode = new IK.SqPackIndex(
                1,
                new KaitaiStream("../../Data/good.index"));
            Action visit = () =>
                SqPackParser.VisitIndex(indexNode, null);

            visit.Should().Throw<ArgumentNullException>(
                "because 'indexFileInfo' is a required parameter")
                .And.ParamName.Should().Be(
                    "indexFileInfo",
                    "because we describe our exceptions");
        }
    }
}
