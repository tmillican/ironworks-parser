namespace IronWorks.Parser.Test
{
    using System;

    using FluentAssertions;
    using Xunit;

    using IronWorks.Kaitai;
    using IronWorks.Parser;

    /// <summary>
    ///   Tests for <see cref="SqPackDataStreamParser"/>.
    /// </summary>
    public static class SqPackDataStreamParserTests
    {
        // ISqPackDataStream Parse(
        //     string dataVolumeFileName,
        //     long dataOffset)
        // ---------------------------------------------------------------------

        [Fact]
        public static void
        Parse_ShouldThrowArgumentNullException_WithNullDataVolumeFileName()
        {
            Action parse = () => SqPackDataStreamParser.Parse(
                null, 0);

            parse.Should().Throw<ArgumentNullException>(
                "because 'dataVolumeFileName' is a required parameter")
                .And.ParamName.Should().Be(
                    "dataVolumeFileName",
                    "because we describe our exceptions");
        }

        [Fact]
        public static void
        Parse_ShouldThrowArgumentOutOfRangeException_WithNegativeDataOffset()
        {
            Action parse = () => SqPackDataStreamParser.Parse(
                "foo", -1);

            parse.Should().Throw<ArgumentOutOfRangeException>(
                "because file offses cannot be negative")
                .WithMessage(
                    $"{Strings.ArgumentOutOfRange_NegativeFileOffset}*",
                    "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "dataOffset",
                    "because we fully describe our exceptions");
        }

        // ISqPackDataStream VisitBinaryStream(
        //     IK.SqPackBinaryStream node)
        // ---------------------------------------------------------------------

        [Fact]
        public static void
        VisitBinaryStream_ShouldThrowNullArgumentException_WithNullNode()
        {
            Action visit = () =>
                SqPackDataStreamParser.VisitBinaryStream(null, 0);

            visit.Should().Throw<ArgumentNullException>(
                "because 'node' is a required parameter")
                .And.ParamName.Should().Be(
                    "node",
                    "because we describe our exceptions");
        }

        // ISqPackDataBlock VisitDataBlock(
        //     IK.SqPackDataBlock node)
        // ---------------------------------------------------------------------

        [Fact]
        public static void
        VisitDataBlock_ShouldThrowNullArgumentException_WithNullNode()
        {
            Action visit = () =>
                SqPackDataStreamParser.VisitDataBlock(null);

            visit.Should().Throw<ArgumentNullException>(
                "because 'node' is a required parameter")
                .And.ParamName.Should().Be(
                    "node",
                    "because we describe our exceptions");
        }

        // ISqPackDataStream VisitModelStream(
        //     IK.SqPackModelStream node)
        // ---------------------------------------------------------------------

        [Fact]
        public static void
        VisitModelStream_ShouldThrowNullArgumentException_WithNullNode()
        {
            Action visit = () =>
                SqPackDataStreamParser.VisitModelStream(null, 0);

            visit.Should().Throw<ArgumentNullException>(
                "because 'node' is a required parameter")
                .And.ParamName.Should().Be(
                    "node",
                    "because we describe our exceptions");
        }

        // ISqPackDataStream VisitTextureStream(
        //     IK.SqPackTextureStream node)
        // ---------------------------------------------------------------------

        [Fact]
        public static void
        VisitTextureStream_ShouldThrowNullArgumentException_WithNullNode()
        {
            Action visit = () =>
                SqPackDataStreamParser.VisitTextureStream(null, 0);

            visit.Should().Throw<ArgumentNullException>(
                "because 'node' is a required parameter")
                .And.ParamName.Should().Be(
                    "node",
                    "because we describe our exceptions");
        }

        // ISqPackDataStream VisitStoredFileStream(
        //     IK.SqPackStoredFileStream node)
        // ---------------------------------------------------------------------

        [Fact]
        public static void
        VisitStoredFileStream_ShouldThrowNullArgumentException_WithNullNode()
        {
            Action visit = () =>
                SqPackDataStreamParser.VisitStoredFileStream(null);

            visit.Should().Throw<ArgumentNullException>(
                "because 'node' is a required parameter")
                .And.ParamName.Should().Be(
                    "node",
                    "because we describe our exceptions");
        }
    }
}
