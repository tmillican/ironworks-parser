namespace IronWorks.Parser
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using global::Kaitai;

    using IK = IronWorks.Kaitai;
    using SFS = IronWorks.Kaitai.SqPackStoredFileStream;

    using IronWorks.Structures;

    /// <summary>
    ///   Parser that produces <see cref="ISqPackDataStream"/> instances.
    /// </summary>
    public static class SqPackDataStreamParser
    {
        /// <summary>
        ///   Produces an <see cref="ISqPackDataStream"/> instance from the
        ///   specified data volume file (.dat file) and offset.
        /// </summary>
        /// <param name="dataVolumeFileName">
        ///   A string containing the data volume's file name.
        /// </param>
        /// <param name="dataOffset">
        ///   The offset to the data stream, in bytes, relative to the beginning
        ///   of the data volume file.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="dataVolumeFileName"/> is <code>null</code>.
        /// </throws>
        /// <throws cref="ArgumentOutOfRangeException">
        ///   <paramref name="dataOffset"/> is negative.
        /// </throws>
        /// <returns>
        ///   An <see cref="ISqPackDataStream"/> instance produced by parsing
        ///   the specified data volume file.
        /// </returns>
        public static ISqPackDataStream Parse(
            string dataVolumeFileName,
            long dataOffset)
        {
            if (dataVolumeFileName is null)
            {
                throw new ArgumentNullException(nameof(dataVolumeFileName));
            }
            if (dataOffset < 0)
            {
                throw new ArgumentOutOfRangeException(
                    nameof(dataOffset),
                    Strings.ArgumentOutOfRange_NegativeFileOffset);
            }

            var kStream = new KaitaiStream(dataVolumeFileName);
            kStream.Seek(dataOffset);
            var dataStreamNode =
                new IK.SqPackStoredFileStream(dataOffset, kStream);

            return VisitStoredFileStream(dataStreamNode);
        }

        /// <summary>
        ///   Produces a value of type <see cref="ISqPackDataStream"/> by
        ///   visiting a concrete syntax node of type <see
        ///   cref="IK.SqPackBinaryStream"/>.
        /// </summary>
        /// <param name="node">
        ///   The <see cref="IK.SqPackBinaryStream"/> node to visit.
        /// </param>
        /// <param name="uncompressedSize">
        ///   The uncompressed size, in bytes, of the binary data stream.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="node"/> is <code>null</code>.
        /// </throws>
        /// <returns>
        ///   A value of type <see cref="ISqPackDataStream"/>
        ///   prodcued by visiting <paramref name="node"/>.
        /// </returns>
        [CLSCompliant(false)]
        public static ISqPackDataStream VisitBinaryStream(
            IK.SqPackBinaryStream node,
            int uncompressedSize)
        {
            if (node is null)
            {
                throw new ArgumentNullException(nameof(node));
            }

            var blocks = new List<ISqPackDataBlock>(node.Blocks.Count);
            for (var i = 0; i < node.Blocks.Count; ++i)
            {
                blocks.Add(
                    VisitDataBlock(
                        node.Blocks[i].Block));
            }

            return new SqPackBinaryDataStream(
                uncompressedSize,
                blocks);
        }

        /// <summary>
        ///   Produces a value of type <see cref="ISqPackDataBlock"/> by
        ///   visiting a concrete syntax node of type <see
        ///   cref="IK.SqPackDataBlock"/>.
        /// </summary>
        /// <param name="node">
        ///   The <see cref="IK.SqPackDataBlock"/> node to visit.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="node"/> is <code>null</code>.
        /// </throws>
        /// <returns>
        ///   A value of type <see cref="ISqPackDataBlock"/>
        ///   prodcued by visiting <paramref name="node"/>.
        /// </returns>
        [CLSCompliant(false)]
        public static ISqPackDataBlock VisitDataBlock(
            IK.SqPackDataBlock node)
        {
            if (node is null)
            {
                throw new ArgumentNullException(nameof(node));
            }

            return new SqPackDataBlock(
                node.IsCompressed,
                node.Metadata.UncompressedSize,
                node.Data);
        }

        /// <summary>
        ///   Produces a value of type <see cref="ISqPackDataStream"/> by
        ///   visiting a concrete syntax node of type <see
        ///   cref="IK.SqPackModelStream"/>.
        /// </summary>
        /// <param name="node">
        ///   The <see cref="IK.SqPackModelStream"/> node to visit.
        /// </param>
        /// <param name="uncompressedSize">
        ///   The uncompressed size, in bytes, of the model data stream.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="node"/> is <code>null</code>.
        /// </throws>
        /// <returns>
        ///   A value of type <see cref="ISqPackDataStream"/>
        ///   prodcued by visiting <paramref name="node"/>.
        /// </returns>
        [CLSCompliant(false)]
        public static ISqPackDataStream VisitModelStream(
            IK.SqPackModelStream node,
            int uncompressedSize)
        {
            if (node is null)
            {
                throw new ArgumentNullException(nameof(node));
            }

            var blockCount = 0;
            for (var i = 0; i < node.Frames.Count; ++i)
            {
                blockCount += node.Frames[i].Blocks.Count;
            }
            var blocks = new List<ISqPackDataBlock>(blockCount);

            for (var i = 0; i < node.Frames.Count; ++i)
            {
                for (var j = 0; j < node.Frames[i].Blocks.Count; ++j)
                {
                    blocks.Add(
                        VisitDataBlock(
                            node.Frames[i].Blocks[j]));
                }
            }

            return new SqPackModelDataStream(
                uncompressedSize,
                blocks);
        }

        /// <summary>
        ///   Produces a value of type <see cref="ISqPackDataStream"/> by
        ///   visiting a concrete syntax node of type <see
        ///   cref="IK.SqPackTextureStream"/>.
        /// </summary>
        /// <param name="node">
        ///   The <see cref="IK.SqPackTextureStream"/> node to visit.
        /// </param>
        /// <param name="uncompressedSize">
        ///   The uncompressed size, in bytes, of the texture data stream.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="node"/> is <code>null</code>.
        /// </throws>
        /// <returns>
        ///   A value of type <see cref="ISqPackDataStream"/>
        ///   prodcued by visiting <paramref name="node"/>.
        /// </returns>
        [CLSCompliant(false)]
        public static ISqPackDataStream VisitTextureStream(
            IK.SqPackTextureStream node,
            int uncompressedSize)
        {
            if (node is null)
            {
                throw new ArgumentNullException(nameof(node));
            }

            var blockCount = 0;
            for (var i = 0; i < node.Frames.Count; ++i)
            {
                blockCount += node.Frames[i].Blocks.Count;
            }
            var blocks = new List<ISqPackDataBlock>(blockCount);

            for (var i = 0; i < node.Frames.Count; ++i)
            {
                for (var j = 0; j < node.Frames[i].Blocks.Count; ++j)
                {
                    blocks.Add(
                        VisitDataBlock(
                            node.Frames[i].Blocks[j]));
                }
            }

            return new SqPackTextureDataStream(
                uncompressedSize,
                node.OriginalFileHeader,
                blocks);
        }

        /// <summary>
        ///   Produces a value of type <see cref="ISqPackDataStream"/> by
        ///   visiting a concrete syntax node of type <see
        ///   cref="IK.SqPackStoredFileStream"/>.
        /// </summary>
        /// <param name="node">
        ///   The <see cref="IK.SqPackStoredFileStream"/> node to visit.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="node"/> is <code>null</code>.
        /// </throws>
        /// <returns>
        ///   A value of type <see cref="ISqPackDataStream"/>
        ///   prodcued by visiting <paramref name="node"/>.
        /// </returns>
        [CLSCompliant(false)]
        public static ISqPackDataStream VisitStoredFileStream(
            IK.SqPackStoredFileStream node)
        {
            if (node is null)
            {
                throw new ArgumentNullException(nameof(node));
            }

            switch (node.StreamType)
            {
                case SFS.ContentType.Binary:
                    return VisitBinaryStream(
                        (IK.SqPackBinaryStream)node.DataStream,
                        node.UncompressedSize);
                case SFS.ContentType.Model:
                    return VisitModelStream(
                        (IK.SqPackModelStream)node.DataStream,
                        node.UncompressedSize);
                case SFS.ContentType.Texture:
                    return VisitTextureStream(
                        (IK.SqPackTextureStream)node.DataStream,
                        node.UncompressedSize);
                default:
                    // What to do here?
                    throw new NotSupportedException();
            }
        }
    }
}
