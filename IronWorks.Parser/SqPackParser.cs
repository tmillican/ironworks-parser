namespace IronWorks.Parser
{
    using System;
    using System.IO;

    using global::Kaitai;

    using IK = IronWorks.Kaitai;
    using IronWorks.Structures;

    /// <summary>
    ///   Parser that produces <see cref="ISqPack"/> instances.
    /// </summary>
    public static class SqPackParser
    {
        /// <summary>
        ///   Produces an <see cref="ISqPack"/> instance
        ///   from the specified index file.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     The entire object graph rooted at the <see cref="ISqPack"/>
        ///     instance will be either mutable or immutable according to the
        ///     <paramref name="makeMutable"/> setting.
        ///   </para>
        ///   <para>
        ///     By themselves, the implementations in
        ///     <code>IronWorks.Structures</code> only guarantee shallow
        ///     immutability (ex. an <see cref="ISqPackFolder"/> is an immutable
        ///     collection of <see cref="ISqPackFile"/> instances which may
        ///     themselves be mutable). Thus it is possible to mix and match
        ///     mutable and immutable data structures when parsing. However,
        ///     this is an edge case that I don't wish to support in
        ///     <code>IronWorks.Parser</code>. To accomplish this, you will need
        ///     to construct your own visitor/parser combinator using
        ///     <code>IronWorks.Kaitai</code> instead. You may find the code
        ///     here useful as guide for how you might do so.
        ///   </para>
        /// </remarks>
        /// <param name="indexFileName">
        ///   A string containing the index file's name.
        /// </param>
        /// <param name="indexFormat">
        ///   A value of type <see cref="SqPackIndexFormat"/> indicating whether
        ///   this is an ".index" or ".index2" format file. While this can be
        ///   inferred from the file extension, this option allows for
        ///   processing renamed indexed files.
        /// </param>
        /// <param name="makeMutable">
        ///   A value indicating whether the <see
        ///   cref="IronWorks.Structures.ISqPack"/> returned should be mutable.
        ///   If ommitted, the default value is <code>false</code>.
        /// </param>
        /// <throws cref="ArgumentException">
        ///   <paramref name="indexFormat"/> isn't a defined <see
        ///   cref="SqPackIndexFormat"/> enum value.
        /// </throws>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="indexFileName"/> is <code>null</code>.
        /// </throws>
        /// <returns>
        ///   An <see cref="IronWorks.Structures.ISqPack"/> instance produced by
        ///   parsing the specified index file.
        /// </returns>
        public static ISqPack Parse(
            string indexFileName,
            SqPackIndexFormat indexFormat,
            bool makeMutable = false)
        {
            if (indexFileName is null)
            {
                throw new ArgumentNullException(nameof(indexFileName));
            }
            if (!Enum.IsDefined(typeof(SqPackIndexFormat), indexFormat))
            {
                throw new ArgumentException(
                    string.Format(
                        Strings.Argument_UnsupportedIndexFormat,
                        (byte)indexFormat),
                    nameof(indexFormat));
            }

            var indexFileInfo = new FileInfo(indexFileName);
            var indexNode = new IK.SqPackIndex(
                (byte)indexFormat,
                new KaitaiStream(indexFileName));

            return VisitIndex(indexNode, indexFileInfo, makeMutable);
        }

        /// <summary>
        ///   Produces a value of type <see cref="ISqPack"/> by visiting a
        ///   concrete syntax node of type <see cref="IK.SqPackIndex"/>.
        /// </summary>
        /// <param name="node">
        ///   The <see cref="IK.SqPackIndex"/> node to visit.
        /// </param>
        /// <param name="indexFileInfo">
        ///   A <see cref="FileInfo"/> object representing the source index
        ///   file.
        /// </param>
        /// <param name="makeMutable">
        ///   A value indicating whether the returned value should be mutable.
        ///   If ommitted, the default value is <code>false</code>.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramname ref="node"/> or <paramname ref="indexFileInfo"/> is
        ///   <code>null</code>.
        /// </throws>
        /// <returns>
        ///   A value of type <see cref="ISqPack"/> prodcued by visiting
        ///   <paramref name="node"/>. This value will have the property
        ///   <code>IsMutable == true</code> if <paramref name="makeMutable"/>
        ///   is <code>true</code>.
        /// </returns>
        [CLSCompliant(false)]
        public static ISqPack VisitIndex(
            IK.SqPackIndex node,
            FileInfo indexFileInfo,
            bool makeMutable = false)
        {
            if (node is null)
            {
                throw new ArgumentNullException(nameof(node));
            }
            if (indexFileInfo is null)
            {
                throw new ArgumentNullException(nameof(indexFileInfo));
            }

            var builder = new SqPack.Builder(
                indexFileInfo.Directory,
                Path.GetFileNameWithoutExtension(indexFileInfo.Name));

            if (node.IndexType == 1)
            {
                foreach (var folderNode in node.FolderSegment.Entries)
                {
                    ISqPackFolder folder =
                        VisitFolderDescriptor(
                            folderNode,
                            makeMutable);
                    builder.Add(folder);
                }
            }
            else
            {
                builder.Add(
                    VisitFileTable(
                        node.FileSegment,
                        0,
                        makeMutable));
            }

            if (makeMutable)
            {
                return builder;
            }
            return builder.ToImmutable();
        }

        /// <summary>
        ///   Produces a value of type <see cref="ISqPackFolder"/> by visiting a
        ///   concrete syntax node of type <see
        ///   cref="IK.SqPackIndex.FileTable"/>.
        /// </summary>
        /// <param name="node">
        ///   The <see cref="IK.SqPackIndex.FileTable"/> node to visit.
        /// </param>
        /// <param name="nameHash">
        ///   A 32-bit name hash for the folder.
        /// </param>
        /// <param name="makeMutable">
        ///   A value indicating whether the returned value should be mutable.
        ///   If ommitted, the default value is <code>false</code>.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramname ref="node"/> is <code>null</code>.
        /// </throws>
        /// <throws cref="ArgumentOutOfRangeException">
        ///   <paramref name="nameHash"/> is less than <code>0</code> or
        ///   greater than <code>UInt.MaxValue</code>.
        /// </throws>
        /// <returns>
        ///   A value of type <see cref="ISqPackFolder"/> prodcued by visiting
        ///   <paramref name="node"/>. This value will have the property
        ///   <code>IsMutable == true</code> if <paramref name="makeMutable"/>
        ///   is <code>true</code>.
        /// </returns>
        [CLSCompliant(false)]
        public static ISqPackFolder VisitFileTable(
            IK.SqPackIndex.FileTable node,
            long nameHash,
            bool makeMutable = false)
        {
            if (node is null)
            {
                throw new ArgumentNullException(nameof(node));
            }

            if (nameHash < 0 || nameHash > uint.MaxValue)
            {
                throw new ArgumentOutOfRangeException(
                    nameof(nameHash),
                    string.Format(
                        Strings.ArgumentOutOfRange_Range,
                        0, uint.MaxValue));
            }

            var builder = new SqPackFolder.Builder(
                nameHash);

            foreach (var fileNode in node.Entries)
            {
                ISqPackFile file =
                    VisitFileDescriptor(
                        fileNode,
                        makeMutable);
                builder.Add(file);
            }

            if (makeMutable)
            {
                return builder;
            }
            return builder.ToImmutable();
        }

        /// <summary>
        ///   Produces a value of type <see cref="ISqPackFolder"/> by visiting a
        ///   concrete syntax node of type <see
        ///   cref="IK.SqPackIndex.FolderTable.Descriptor"/>.
        /// </summary>
        /// <param name="node">
        ///   The <see
        ///   cref="IK.SqPackIndex.FolderTable.Descriptor"/> node to visit.
        /// </param>
        /// <param name="makeMutable">
        ///   A value indicating whether the returned value should be mutable.
        ///   If ommitted, the default value is <code>false</code>.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramname ref="node"/> is <code>null</code>.
        /// </throws>
        /// <returns>
        ///   A value of type <see cref="ISqPackFolder"/> prodcued by visiting
        ///   <paramref name="node"/>. This value will have the property
        ///   <code>IsMutable == true</code> if <paramref name="makeMutable"/>
        ///   is <code>true</code>.
        /// </returns>
        [CLSCompliant(false)]
        public static ISqPackFolder VisitFolderDescriptor(
            IK.SqPackIndex.FolderTable.Descriptor node,
            bool makeMutable = false)
        {
            if (node is null)
            {
                throw new ArgumentNullException(nameof(node));
            }

            var builder = new SqPackFolder.Builder(
                node.NameHash);

            foreach (var fileNode in node.Files.Entries)
            {
                ISqPackFile file =
                    VisitFileDescriptor(
                        fileNode,
                        makeMutable);
                builder.Add(file);
            }

            if (makeMutable)
            {
                return builder;
            }
            return builder.ToImmutable();
        }

        /// <summary>
        ///   Produces a value of type <see cref="ISqPackFile"/> by visiting a
        ///   concrete syntax node of type <see
        ///   cref="IK.SqPackIndex.FileTable.Descriptor"/>.
        /// </summary>
        /// <param name="node">
        ///   The <see cref="IK.SqPackIndex.FileTable.Descriptor"/>. node to
        ///   visit.
        /// </param>
        /// <param name="makeMutable">
        ///   A value indicating whether the returned value should be immutable.
        ///   If ommitted, the default value is <code>false</code>.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramname ref="node"/> is <code>null</code>.
        /// </throws>
        /// <returns>
        ///   A value of type <see cref="ISqPackFile"/> prodcued by visiting
        ///   <paramref name="node"/>. This value will have the property
        ///   <code>IsMutable == true</code> if <paramref name="makeMutable"/>
        ///   is <code>true</code>.
        /// </returns>
        [CLSCompliant(false)]
        public static ISqPackFile VisitFileDescriptor(
            IK.SqPackIndex.FileTable.Descriptor node,
            bool makeMutable = false)
        {
            if (node is null)
            {
                throw new ArgumentNullException(nameof(node));
            }

            if (makeMutable)
            {
                return new MutableSqPackFile(
                    node.NameHash,
                    (byte)node.DataVolumeId,
                    node.DataOffset);
            }
            return new SqPackFile(
                node.NameHash,
                (byte)node.DataVolumeId,
                node.DataOffset);
        }
    }
}
