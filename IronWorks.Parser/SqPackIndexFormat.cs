namespace IronWorks.Parser
{
    /// <summary>
    ///   An enum describing an SqPack index format.
    /// </summary>
    public enum SqPackIndexFormat : byte
    {
        /// <summary>
        ///   Index format corresponding to ".index" files.
        /// </summary>
        Format1 = 1,

        /// <summary>
        ///   Index format corresponding to ".index2" files.
        /// </summary>
        Format2 = 2,
    }
}
